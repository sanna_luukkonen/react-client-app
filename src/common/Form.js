import { Component } from "react";

/* Reusable form */
class Form extends Component {
  state = {
    data: {},
    errors: {},
    submitStatus: null
  };

  validate = () => {
    const options = { abortEarly: false };
    const { error } = this.schema.validate(this.getValidateObject(), options);

    if (!error) return null;
    const errors = {};
    for (let item of error.details) errors[item.path[0]] = item.message;
    return errors;
  };

  validateProperty = ({ name, value }) => {
    const { error } = this.fieldSchema.validate({ [name]: value });
    return error ? error.details[0].message : null;
  };

  getValidateObject = () => {
    return this.state.data;
  };

  handleSubmit = e => {
    e.preventDefault(); // Prevent default form behaviour

    const errors = this.validate();

    this.setState({ errors: errors || {} });
    if (errors) return;

    this.doSubmit();
  };

  onChange = ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    const errorMessage = this.validateProperty(input);
    if (errorMessage) errors[input.name] = errorMessage;
    else delete errors[input.name];

    const data = { ...this.state.data };
    data[input.name] = input.value;
    this.setState({ data, submitStatus: null, errors });
  };
}
export default Form;
