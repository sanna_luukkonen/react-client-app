import React from "react";
import Input from "../../src/input";
import Joi from "@hapi/joi";
import { Link } from "react-router-dom";
import Form from "./Form";

class ProductForm extends Form {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        name: "",
        description: "",
        price: "",
        original_price: "",
        size: "",
        color: ""
      },
      submitStatus: "",
      errors: {}
    };
  }

  schema = Joi.object({
    name: Joi.string()
      .required()
      .label("Name"),
    description: Joi.string().label("Description"),
    price: Joi.number()
      .required()
      .label("Price"),
    original_price: Joi.number().label("Original price"),
    size: Joi.string().label("Size"),
    color: Joi.string().label("Color")
  });

  fieldSchema = Joi.object({
    name: Joi.string().label("Name"),
    description: Joi.string().label("Description"),
    price: Joi.number().label("Price"),
    original_price: Joi.number().label("Original price"),
    size: Joi.string().label("Size"),
    color: Joi.string().label("Color")
  });

  validateProperty = ({ name, value }) => {
    if (name.localeCompare("price") === 0) {
      const original_price = parseFloat(this.state.data.original_price, 10);
      if (original_price && value > original_price) {
        return "Price must be smaller or equal than original price.";
      }
    }

    if (name.localeCompare("original_price") === 0) {
      const currentPrice = parseFloat(this.state.data.price, 10);
      if (value < currentPrice) {
        return "Original price must be equal or greater than price.";
      }
    }

    const { error } = this.fieldSchema.validate({ [name]: value });
    return error ? error.details[0].message : null;
  };

  render() {
    if (!this.state.data) return <div></div>;

    const token = localStorage.getItem("token");

    if (!token) {
      return <div>You must ge logged in to {this.state.title}</div>;
    }

    const {
      name,
      description,
      price,
      original_price,
      size,
      color
    } = this.state.data;

    const { errors } = this.state;

    return (
      <div className="mx-4 d-block">
        {" "}
        <h1>{this.state.title}</h1>
        <form onSubmit={this.handleSubmit}>
          <Input
            name="name"
            value={name}
            label="Name"
            onChange={this.onChange}
            error={errors.name}
          ></Input>
          <Input
            name="description"
            value={description}
            label="Description"
            onChange={this.onChange}
            error={errors.description}
          ></Input>
          <Input
            name="price"
            value={price}
            label="Current price"
            onChange={this.onChange}
            error={errors.price}
          ></Input>
          <Input
            name="original_price"
            value={original_price}
            label="Original price"
            onChange={this.onChange}
            error={errors.original_price}
          ></Input>
          <Input
            name="size"
            value={size}
            label="Size"
            onChange={this.onChange}
            error={errors.size}
          ></Input>
          <Input
            name="color"
            value={color}
            label="Color"
            onChange={this.onChange}
            error={errors.color}
          ></Input>
          <button className="btn btn-primary">{this.state.buttonLabel}</button>
        </form>
        <div>{this.state.submitStatus}</div>
        <Link to={"/products/"}>Back to product list</Link>
      </div>
    );
  }

  getConfigWithAccessToken = action => {
    const token = localStorage.getItem("token");

    if (!token) {
      this.setState({
        submitStatus: "You must be logged in to " + action + " a product."
      });
      return null;
    }

    const config = {
      headers: {
        "x-auth-token": token
      }
    };
    return config;
  };
}
export default ProductForm;
