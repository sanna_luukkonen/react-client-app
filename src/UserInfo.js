import React from "react";

const UserInfo = ({ token, onLogout }) => {
  if (!token) {
    return (
      <div className="alert alert-warning">
        You are not logged in. Login at the login page or register as a new user
        at the register page.
      </div>
    );
  } else {
    return (
      <div className="alert alert-dark">
        You are logged in.{" "}
        <button className="btn m-2 btn-warning" onClick={onLogout}>
          Logout
        </button>
      </div>
    );
  }
};

export default UserInfo;
