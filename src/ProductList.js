import React from "react";
import Product from "./Product";

const ProductList = props => {
  const { products, onDelete } = props;
  if (products.length > 0) {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>Product name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Original Price</th>
            <th>Size</th>
            <th>Color</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {products.map(prod => (
            <Product key={prod._id} data={prod} onDelete={onDelete} />
          ))}
        </tbody>
      </table>
    );
  } else {
    return null;
  }
};

export default ProductList;
