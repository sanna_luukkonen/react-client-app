import React from "react";
import { Link } from "react-router-dom";

const Product = props => {
  const { data, onDelete } = props;
  return (
    <tr>
      <td>
        <Link to={"/products/" + data._id}>{data.name}</Link>
      </td>
      <td>{data.description}</td>
      <td>{data.price}</td>
      <td>{data.original_price}</td>

      <td>{data.size}</td>
      <td>{data.color}</td>
      <td>
        <button
          className="btn btn-danger btn-sm"
          onClick={() => {
            onDelete(data._id);
          }}
        >
          X
        </button>
      </td>
    </tr>
  );
};

export default Product;
