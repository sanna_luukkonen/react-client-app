import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import "./App.css";
import ProductList from "./ProductList";
import NavBar from "./navbar";
import Login from "./Login";
import UserInfo from "./UserInfo";
import Register from "./Register";
import ProductAddView from "./ProductAddView";
import ProductUpdateView from "./ProductUpdateView";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: []
    };
  }

  render() {
    return (
      <div>
        <UserInfo
          token={localStorage.getItem("token")}
          onLogout={this.handleLogout}
        ></UserInfo>
        <NavBar></NavBar>
        <Switch>
          <Route
            path="/register"
            render={props => (
              <Register {...props} onUpdate={this.getAllProducts}></Register>
            )}
          ></Route>
          <Route
            path="/login"
            render={props => (
              <Login {...props} onUpdate={this.getAllProducts}></Login>
            )}
          ></Route>
          <Route
            path="/add"
            render={props => (
              <ProductAddView
                {...props}
                onUpdate={this.getAllProducts}
              ></ProductAddView>
            )}
          ></Route>
          <Route
            path="/products/:id"
            render={props => (
              <ProductUpdateView
                {...props}
                onUpdate={this.getAllProducts}
              ></ProductUpdateView>
            )}
          ></Route>
          <Route
            path="/"
            render={() => (
              <ProductList
                products={this.state.products}
                onDelete={this.handleProductDelete}
              ></ProductList>
            )}
          ></Route>
        </Switch>
      </div>
    );
  }

  componentDidMount() {
    this.getAllProducts();
  }

  getAllProducts = () => {
    axios
      .get("http://localhost:3000/api/products")
      .then(response => {
        this.setState({ products: response.data });
      })
      .catch(error => {
        console.log(error);
      });
  };

  handleLogout = () => {
    localStorage.removeItem("token");
    this.setState({ account: { name: "", email: "" } });
  };

  // Handle product delete
  handleProductDelete = e => {
    const token = localStorage.getItem("token");

    if (!token) {
      console.log("You must be logged in to delete a product!");
      return;
    }

    const config = {
      headers: {
        "x-auth-token": token
      }
    };

    axios.delete("http://localhost:3000/api/products/" + e, config).then(
      response => {
        this.getAllProducts();
      },
      error => {
        console.log(error);
      }
    );
  };
}
export default App;
