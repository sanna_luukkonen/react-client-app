import React from "react";
import { Link } from "react-router-dom";

const NavBar = () => {
  return (
    <ul className="navigation">
      <li>
        <Link to="/">Products</Link>
      </li>
      <li>
        <Link to="/add">Add product</Link>
      </li>
      <li>
        <Link to="/register">Register</Link>
      </li>
      <li>
        <Link to="/login">Login</Link>
      </li>
    </ul>
  );
};
export default NavBar;
