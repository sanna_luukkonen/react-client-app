import axios from "axios";
import _ from "lodash";
import ProductForm from "./common/ProductForm";

/***************************************
 View for updating product details 
 ***************************************/
class ProductUpdateView extends ProductForm {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        name: "",
        description: "",
        price: "",
        original_price: "",
        size: "",
        color: ""
      },
      title: "Update product details",
      buttonLabel: "Update product",
      errors: {}
    };
  }

  getValidateObject = () => {
    // Remove _id and __v
    return _.omit(this.state.data, ["_id", "__v"]);
  };

  componentDidMount() {
    axios
      .get("http://localhost:3000/api/products/" + this.props.match.params.id)
      .then(response => {
        this.setState({ data: response.data });
      })
      .catch(error => {
        this.setState({ submitStatus: "Unable to get product data." });
      });
  }

  doSubmit = () => {
    const config = this.getConfigWithAccessToken("update");

    if (!config) return;

    const prod = _.omit(this.state.data, ["_id", "__v"]);

    axios
      .put(
        "http://localhost:3000/api/products/" + this.state.data._id,
        prod,
        config
      )
      .then(
        response => {
          this.setState({ submitStatus: "Product was updated successfully." });
          this.props.onUpdate();
        },
        error => {
          this.setState({ submitStatus: "Product was NOT updated." });
        }
      );
  };
}

export default ProductUpdateView;
