import axios from "axios";
import ProductForm from "./common/ProductForm";

class ProductAddView extends ProductForm {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        name: "",
        description: "",
        price: "",
        original_price: "",
        size: "",
        color: ""
      },
      title: "Add new product",
      buttonLabel: "Add product",
      errors: {}
    };
  }

  // Handle adding a product, a JSON Web token must be stored in local storage
  doSubmit = () => {
    const config = this.getConfigWithAccessToken("add");

    if (!config) return;

    axios
      .post("http://localhost:3000/api/products", this.state.data, config)
      .then(
        response => {
          this.setState({ submitStatus: "Product was added successfully." });
          this.props.onUpdate();
        },
        error => {
          this.setState({ submitStatus: "Product was not added." });
        }
      );
  };
}
export default ProductAddView;
