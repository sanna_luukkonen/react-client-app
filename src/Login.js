import React from "react";
import Input from "./input";
import axios from "axios";
import Joi from "@hapi/joi";
import Form from "./common/Form";

class Login extends Form {
  constructor(props) {
    super(props);
    this.state = {
      data: { email: "", password: "" },
      errors: {},
      submitStatus: null
    };
  }

  schema = Joi.object({
    email: Joi.string()
      .email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } })
      .required()
      .label("Email"),
    password: Joi.string()
      .required()
      .label("Password")
  });

  fieldSchema = Joi.object({
    email: Joi.string()
      .email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } })
      .label("Email"),
    password: Joi.string().label("Password")
  });

  render() {
    const { data: account, errors } = this.state;
    return (
      <div className="mx-4 d-block">
        <h1>Login</h1>
        <form onSubmit={this.handleSubmit}>
          <Input
            name="email"
            value={account.email || ""}
            label="Email"
            onChange={this.onChange}
            error={errors.email}
          ></Input>
          <Input
            name="password"
            value={account.password || ""}
            label="Password"
            onChange={this.onChange}
            error={errors.password}
          ></Input>
          <button className="btn btn-primary">Login</button>
          {this.state.submitStatus && <div>{this.state.submitStatus}</div>}
        </form>
      </div>
    );
  }

  doSubmit = () => {
    axios.post("http://localhost:3000/api/auth", this.state.data).then(
      response => {
        // Save x-auth-token and user name
        const data = { ...this.state.data };
        //account["name"] = response.data.name;
        localStorage.setItem("token", response.headers["x-auth-token"]);
        this.setState({ data: data, submitStatus: "Login successfull." });
        this.props.onUpdate();
      },
      error => {
        this.setState({ submitStatus: "Invalid email or password." });
      }
    );
  };
}

export default Login;
