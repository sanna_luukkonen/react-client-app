import React from "react";
import Input from "./input";
import Form from "./common/Form";
import axios from "axios";
import Joi from "@hapi/joi";

class Register extends Form {
  constructor(props) {
    super(props);
    this.state = {
      data: { email: "", password: "", name: "" },
      errors: {},
      submitStatus: null
    };
  }

  schema = Joi.object({
    email: Joi.string()
      .email({ minDomainSegments: 2, tlds: { allow: ["com", "net","fi"] } })
      .required()
      .label("Email"),
    password: Joi.string()
      .required()
      .label("Password"),
    name: Joi.string()
      .required()
      .label("Name")
  });

  fieldSchema = Joi.object({
    email: Joi.string()
      .email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } })
      .label("Email"),
    password: Joi.string().label("Password"),
    name: Joi.string().label("Name")
  });

  render() {
    const { data, errors } = this.state;
    return (
      <div className="mx-4 d-block">
        <h1>Register</h1>
        <form onSubmit={this.handleSubmit}>
          <Input
            name="email"
            value={data.email}
            label="Email"
            onChange={this.onChange}
            error={errors.email}
          ></Input>
          <Input
            name="password"
            value={data.password}
            label="Password"
            onChange={this.onChange}
            error={errors.password}
          ></Input>
          <Input
            name="name"
            value={data.name}
            label="Name"
            onChange={this.onChange}
            error={errors.name}
          ></Input>

          <button className="btn btn-primary">Register</button>
          {this.state.submitStatus && <div>{this.state.submitStatus}</div>}
        </form>
      </div>
    );
  }

  // Handle register request by calling /api/users endpoint (POST request)
  doSubmit = () => {
    axios.post("http://localhost:3000/api/users", this.state.data).then(
      response => {
        // Save x-auth-token
        localStorage.setItem("token", response.headers["x-auth-token"]);
        this.setState({ submitStatus: "User registered successfully." });
      },
      error => {
        this.setState({ submitStatus: "User already registered." });
      }
    );
  };
}
export default Register;
